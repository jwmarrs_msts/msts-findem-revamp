FROM ruby:2.2.3

RUN mkdir /findem
WORKDIR   /findem
ADD .     /findem

RUN bundle install --path vendor/bundle --without local development
RUN rake assets:precompile
EXPOSE 3000

CMD ["/bin/bash", "-c", "bundle exec rails s -b 0.0.0.0"]
