# encoding: utf-8
require 'csv'
# rake import_file:employees

# [0, "Employee_ID"]
# [1, "Workday Account"]
# [2, "Worker"]
# [3, "Legal Name - First Name"]
# [4, "Legal Name - Last Name"]
# [5, "Manager"]
# [6, "Company"]
# [7, "Public Work Address - Full with Country"]
# [8, "Work E-mail"]
# [9, "Work Telephone"]
# [10, "BLDG"]
# [11, "Floor"]
# [12, "Extension"]
# [13, "Location"]

namespace :import_file do
  desc "IMPORT Employee Information from csv file"
  task :employees => :environment do
    Employee.delete_all
    CSV.foreach('Employee_Work_Information.csv') do |row|
      next if row[0] == 'Employee_ID'
      first_name = row[2]
      last_name = row[3]
      current_employee = true
      email_address = row[10]
      phone_number = row[9]
      # extension = row[12]
      Employee.create(
        first_name: first_name,
        last_name: last_name,
        current_employee: current_employee,
        email: email_address,
        phone_number: phone_number,
        # extension: extension,
      )
    end
  end
end
