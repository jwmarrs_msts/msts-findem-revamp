require 'test_helper'

class LocationsControllerTest < ActionController::TestCase
  setup do
    @location = locations(:one)
  end

  test "should get show" do
    get :show, id: @location
    assert_response :success
  end

end
