require 'test_helper'

class ConfRoomsControllerTest < ActionController::TestCase
  setup do
    @conf_room = conf_rooms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:conf_rooms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create conf_room" do
    assert_difference('ConfRoom.count') do
      post :create, conf_room: {:location_id => @conf_room.location_id, :x_cord => @conf_room.x_cord, :y_cord => @conf_room.y_cord}
    end

    assert_redirected_to conf_room_path(assigns(:conf_room))
  end

  test "should show conf_room" do
    get :show, id: @conf_room
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @conf_room
    assert_response :success
  end

  test "should update conf_room" do
    patch :update, id: @conf_room, conf_room: {location_id: @conf_room.location_id}
    assert_redirected_to conf_room_path(assigns(:conf_room))
  end

  test "should destroy conf_room" do
    assert_difference('ConfRoom.count', -1) do
      delete :destroy, id: @conf_room
    end

    assert_redirected_to conf_rooms_path
  end
end
