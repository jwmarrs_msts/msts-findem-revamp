PointlessFeedback.setup do |config|
  # ==> Feedback Configuration
  # Configure the topics for the user to choose from on the feedback form
  config.message_topics = ['Error on page', "What I'd Really Like to See", 'Kudos', "I'd like to help", 'Other']

  # ==> Email Configuration
  # Configure feedback email properties (disabled by default)
  # Variables needed for emailing feedback
  config.email_feedback            = true
  # config.send_from_submitter       = false
  config.from_email                = 'feedback@pointlesscorp.com'
  config.to_emails                 = ['rwdonnard@msts.com']

  # config.google_captcha_site_key   = nil
  # config.google_captcha_secret_key = nil
end
