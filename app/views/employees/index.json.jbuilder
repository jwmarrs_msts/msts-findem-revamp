json.array!(@employees) do |employee|
  json.extract! employee, :id, :email, :current_employee, :first_name, :last_name, :building, :x_cord, :y_cord
  json.url employee_url(employee, format: :json)
end
