class SearchController < ApplicationController
  def index
  end

  def show
    name_lower = '%' +(params[:emp_name].downcase) + '%'
    @employees = Employee.where("current_employee = 't'").
        where("lower(first_name) like ? or lower(last_name) like ? or lower(email) like ? or (lower(first_name) || ' ' || lower(last_name) like ?)",
        name_lower, name_lower, name_lower, name_lower).page(params[:page]).order("first_name ASC, last_name ASC")
    @conf_rooms = ConfRoom.where("lower(conf_room) like ?", name_lower).order("conf_room ASC")
    if !@employees.many? && !@employees.empty? && @conf_rooms.empty?
        redirect_to controller: 'employees', action: 'show', id: @employees.first
    end
    if !@conf_rooms.many? && !@conf_rooms.empty? && @employees.empty?
        redirect_to controller: 'conf_rooms', action: 'show', id: @conf_rooms.first
    end
  end
end
