class LocationsController < ApplicationController
  def index
    @locations = Location.where(has_seats: 'Y').order('location')
    respond_to do |format|
      format.html
      format.json { render :json => @locations }
    end
  end

  def show
    @location = Location.where(id: params[:id]).first
    @employees = Employee.where(location_id: params[:id]).where("current_employee = 't'").page(params[:page]).order("first_name ASC, last_name ASC")
    @employees2 = Employee.where(location_id: params[:id]).where("current_employee = 't'")
    @conf_rooms = ConfRoom.where(location_id: params[:id])
    respond_to do |format|
      format.html
      format.json { render :json => @location }
    end
  end

  def new
  end
end
