class ConfRoomsController < ApplicationController
  before_action :set_conf_room, only: [:show, :edit, :update, :destroy]
  before_action :load_locations, only: [:new, :edit, :create, :update]
  protect_from_forgery :except => [:update]

  # GET /conf_rooms
  # GET /conf_rooms.json
  def index
    @conf_rooms = ConfRoom.all.order("conf_room ASC")
  end

  # GET /conf_rooms/1
  # GET /conf_rooms/1.json
  def show
    @location = @conf_room.building
  end

  # GET /conf_rooms/new
  def new
    @conf_room = ConfRoom.new
  end

  # GET /conf_rooms/1/edit
  def edit
  end

  # POST /conf_rooms
  # POST /conf_rooms.json
  def create
    @conf_room = ConfRoom.new(conf_room_params)

    respond_to do |format|
      if @conf_room.save
        format.html { redirect_to @conf_room, notice: 'Conf room was successfully created.' }
        format.json { render :show, status: :created, location: @conf_room }
      else
        format.html { render :new }
        format.json { render json: @conf_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conf_rooms/1
  # PATCH/PUT /conf_rooms/1.json
  def update
    respond_to do |format|
      if @conf_room.update(conf_room_params)
        format.html { redirect_to @conf_room, notice: 'Conf room was successfully updated.' }
        format.json { render :show, status: :ok, location: @conf_room }
      else
        format.html { render :edit }
        format.json { render json: @conf_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conf_rooms/1
  # DELETE /conf_rooms/1.json
  def destroy
    @conf_room.destroy
    respond_to do |format|
      format.html { redirect_to conf_rooms_url, notice: 'Conf room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conf_room
      @conf_room = ConfRoom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conf_room_params
      params.require(:conf_room).permit(:conf_room, :location_id, :x_cord, :y_cord)
    end

    def load_locations
      @locations = Location.where(has_seats: 'Y').order('location').pluck(:location, :id)
    end
end
