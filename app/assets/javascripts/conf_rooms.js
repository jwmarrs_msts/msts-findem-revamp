//= require jquery

Vue.http.interceptors.push({
  request: function (request) {
    Vue.http.headers.common['X-CSRF-Token'] = $('[name="csrf-token"]').attr('content');
    return request;
  },
  response: function (response) {
    return response;
  }
});

var confRoomResource = Vue.resource(window.location + '.json');

var app = new Vue({
  el: '#info',
  props: {
  },
  data: {
    editMode: false,
    conf_room: {
      id: '',
      conf_room: '',
      building: '',  // This is the name of the building, for display purpose
      location: '',  // This is the building ID, for DB storage
      x_cord: 0,
      y_cord: 0,
      map_img: '/assets/FindMe.png',
      show_dot: false
    },
    locations: [
    ],
    map_images: [
    ]
  },
  mounted: function() {
    var that = this;

    Vue.http.get('/locations.json').then( function (response) {
      that.locations = [];
      that.map_images = [];

      for (i in response.data) {
        var id = response.data[i].id;
        that.locations[i] = {
          text: response.data[i].location,
          value: id
        };
        that.map_images[id] = '/assets/' + response.data[i].image;
      }

      confRoomResource.get().then( function (response) {
        that.conf_room = response.data;
        that.conf_room.location = response.data.location.id;
        that.conf_room.map_img = that.map_images[that.conf_room.location];
      });
    });
  },
  methods: {
    updateConfRoom: function () {
      var loc = document.getElementById("conf_room.location");
      if (loc.selectedIndex == -1) this.conf_room.building = '';
      else this.conf_room.building = loc.options[loc.selectedIndex].text;
      this.editMode = false;
      Vue.http.patch(window.location + '.json', {'conf_room':this.conf_room});
    },
    maphandler: function(event) {
      if (this.editMode) {
        event.preventDefault();
        var x_coord = Math.round( event.pageX - $('#map a img').offset().left ) - 12;
        var y_coord = Math.round( event.pageY - $('#map a img').offset().top ) - 12;
        console.log('X: ' + x_coord + ' | Y: ' + y_coord);
        this.conf_room.x_cord = x_coord;
        this.conf_room.y_cord = y_coord;
      }
    },
    updateMap: function() {
      this.conf_room.x_cord = 0;
      this.conf_room.y_cord = 0;
    }
  }
});

