//= require jquery

Vue.http.interceptors.push({
  request: function (request) {
    Vue.http.headers.common['X-CSRF-Token'] = $('[name="csrf-token"]').attr('content');
    return request;
  },
  response: function (response) {
    return response;
  }
});

var employeeResource = Vue.resource(window.location + '.json');

var app = new Vue({
  el: '#info',
  props: {
  },
  data: {
    editMode: false,
    employee: {
      id: '',
      email: '',
      current_employee: true,
      first_name: '',
      last_name: '',
      phone_number: '',
      extension: '',
      building: '',  // This is the name of the building, for display purpose
      location: '',  // This is the building ID, for DB storage
      x_cord: 0,
      y_cord: 0,
      map_img: '/assets/FindMe.png',
      show_dot: false
    },
    locations: [
    ],
    map_images: [
    ],
    employee_status: '\u2714'
  },
  mounted: function() {
    var that = this;

    Vue.http.get('/locations.json').then( function (response) {
      that.locations = [];
      that.map_images = [];

      for (i in response.data) {
        var id = response.data[i].id;
        that.locations[i] = {
          text: response.data[i].location,
          value: id
        };
        that.map_images[id] = '/assets/' + response.data[i].image;
      }

      employeeResource.get().then( function (response) {
        that.employee = response.data;
        that.employee.location = response.data.location.id;
        if (!that.employee.current_employee) that.employee_status = '\u2717';
        that.employee.map_img = that.map_images[that.employee.location];
      });
    });
  },
  methods: {
    updateEmployee: function () {
      var loc = document.getElementById("employee.location");
      if (loc.selectedIndex == -1) this.employee.building = '';
      else this.employee.building = loc.options[loc.selectedIndex].text;
      this.editMode = false;
      Vue.http.patch(window.location + '.json', {'employee':this.employee});
    },
    maphandler: function(event) {
      if (this.editMode) {
        event.preventDefault();
        var x_coord = Math.round( event.pageX - $('#map a img').offset().left ) - 12;
        var y_coord = Math.round( event.pageY - $('#map a img').offset().top ) - 12;
        console.log('X: ' + x_coord + ' | Y: ' + y_coord);
        this.employee.x_cord = x_coord;
        this.employee.y_cord = y_coord;
      }
    },
    employeeStatus: function() {
      if (this.editMode) {
        if (this.employee.current_employee) {
          this.employee.current_employee = false;
          this.employee_status = '\u2717';
        }
        else {
          this.employee.current_employee = true;
          this.employee_status = '\u2714';
        }
      }
    },
    updateMap: function() {
      this.employee.x_cord = 0;
      this.employee.y_cord = 0;
    }
  }
});

