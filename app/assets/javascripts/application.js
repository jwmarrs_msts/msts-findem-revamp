// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
// require_tree .
//= require vue
//= require vue-resource

$(function() {
    $('#employee_search').on('submit', submit_employee_search);

    function submit_employee_search ( event ) {
        event.preventDefault();
        var submit_url = '/search/' + $('#emp_name').val();
        window.location = submit_url;
    }

    // If there is a DOM object with id of map_edit, that means the page
    // is in new/edit mode, and showing the map.
    // The code below will handle the on-click event on the map image,
    // as well as the on-change event for the location SELECT object
    if ( ( typeof $('#map_edit').get(0) ).toString() == 'object' ) {
        // the actual function
        function update_employee_coord( event ) {
            event.preventDefault();
            var x_coord = Math.round( event.pageX - $('#map_edit a img').offset().left );
            var y_coord = Math.round( event.pageY - $('#map_edit a img').offset().top );
            console.log( 'pageX:' + event.pageX );
            console.log( 'clientX:' + event.clientX );
            console.log( 'offset:' + $('#map_edit').offset().left );

            // update the x/y coordinate values in the form
            $('#x_cord').val(x_coord);
            $('#y_cord').val(y_coord);

            // move the red dot on the map
            var dot_x = x_coord - 12;
            var dot_y = y_coord - 12;
            $('.map_dot').css("top", dot_y).css("left", dot_x).show();
        }

        // call the /locations/:id via ajax to get the location's image name
        function swap_map_init() {
            var location_id = $(this).val();
            if( location_id != '' ) {
                $.ajax( {
                    url:'/locations/' + location_id + '.json',
                    success: retrieve_map_data,
                    dataType: 'json',
                    error: function() { alert ( "Could not retrieve the building info" ); }
                } );
            }
            else {
                url = '/assets/unknown.png';
                swap_map( url );
            }
        }

        // this is the actual function that swaps the image displayed
        function retrieve_map_data( data ) {
            var image = data.image;
            var orientation = data.orientation;
            var url = '/assets/' + image;
            swap_map( url, orientation );
        }

        // this is the actual function that swaps the image displayed
        function swap_map( url, orientation ) {
            $('#map_edit a img').attr('src', url);
            $('.map_dot').css('top', 0).css('left',0).hide();
            $('#x_cord').val(0);
            $('#y_cord').val(0);
            orientation = parseInt( orientation );
            if ( isNaN( orientation ) ) {
                $('.compass_rose').hide();
            }
            else {
                $('.compass_rose').show();
                $('.compass_rose').css('transform', 'rotate('+orientation+'deg)');
            }
        }

        // event listners
        $('#map_edit a').on('click', update_employee_coord);
        $('#location_selector').on('change', swap_map_init);

        // initialize some values
        var map_obj = $('#map_edit').get(0).getBoundingClientRect();
        var map_x = map_obj.left;
        var map_y = map_obj.top;
    }
});

