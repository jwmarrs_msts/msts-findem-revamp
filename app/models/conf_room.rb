class ConfRoom < ActiveRecord::Base
  belongs_to :location

  def building 
    self.location.try(:location)
  end
end
