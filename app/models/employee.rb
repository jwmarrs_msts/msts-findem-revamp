class Employee < ActiveRecord::Base
  self.per_page = 25

  belongs_to :location

  validates :email,  uniqueness: true #, presence: true
  validates :first_name,  presence: true
  validates :last_name,   presence: true

  def building
    self.location.try(:location)
  end
end
