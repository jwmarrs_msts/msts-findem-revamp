class Location < ActiveRecord::Base
  has_many :employees
  has_many :conf_rooms
  validates :location, presence: true, uniqueness: true

  def parent_location
    (self.parent ? Location.find(self.parent) : nil)
  end

  def children
    Location.where("parent = ?",self.id)
  end
end
