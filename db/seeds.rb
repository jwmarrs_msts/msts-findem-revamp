# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Location.delete_all

# Overland Park
opp = Location.create(id: 1, location: 'Overland Park', parent: nil, image: 'opp.png', has_seats: 'N')

p8650 = Location.create(id: 2, location: '8650', parent: opp.id, image: '8650.png', has_seats: 'N')

p8600 = Location.create(id: 3, location: '8600', parent: opp.id, image: '8600.png', has_seats: 'N')

p8595 = Location.create(id: 4, location: '8595', parent: opp.id, image: '8595.png', has_seats: 'N')

p8620 = Location.create(id: 5, location: '8620', parent: opp.id, image: '8620.png', has_seats: 'N')

# Overland Park Buildings
Location.create(id: 6, location: '8650-1', parent: p8650.id, image: '8650-1-bw.png', has_seats: 'Y', orientation: 180)

Location.create(id: 7, location: '8650-2', parent: p8650.id, image: '8650-2-bw.png', has_seats: 'Y', orientation: 180)

Location.create(id: 8, location: '8595-1', parent: p8595.id, image: '8595-1-bw.png', has_seats: 'Y', orientation: 90)

Location.create(id: 9, location: '8620-1', parent: p8620.id, image: '8620-1.png', has_seats: 'Y', orientation: 90)

Location.create(id: 10, location: '8620-2', parent: p8620.id, image: '8620-2.png', has_seats: 'Y', orientation: 90)

# Australia
aup = Location.create(id: 11, location: 'Australia', parent: nil, image: 'au.png', has_seats: 'N')

# Australia splash
Location.create(id: 12, location: 'Melbourne', parent: aup.id, image: 'FindMe.png', has_seats: 'Y')

# Netherlands
nep = Location.create(id: 13, location: 'Netherlands', parent: nil, image: 'ne.png', has_seats: 'N')

# Netherlands splash
Location.create(id: 14, location: 'Rijswijk', parent: nep.id, image: 'FindMe.png', has_seats: 'Y')

ConfRoom.delete_all

ConfRoom.create(conf_room: 'Black Swan',            location_id: 6, x_cord: 208, y_cord: 364)
ConfRoom.create(conf_room: 'Blue Ocean',            location_id: 6, x_cord: 280, y_cord: 173)
ConfRoom.create(conf_room: 'Compendium',            location_id: 6, x_cord: 170, y_cord: 223)
ConfRoom.create(conf_room: 'Craving Brain',         location_id: 6, x_cord: 208, y_cord: 335)
ConfRoom.create(conf_room: 'Delivering Happiness',  location_id: 6, x_cord: 87,  y_cord: 223)
ConfRoom.create(conf_room: 'DRiVe',                 location_id: 6, x_cord: 484, y_cord: 360)
ConfRoom.create(conf_room: 'Gung Ho',               location_id: 6, x_cord: 484, y_cord: 317)
ConfRoom.create(conf_room: 'Lean In',               location_id: 6, x_cord: 498, y_cord: 237)
ConfRoom.create(conf_room: 'Parachute',             location_id: 6, x_cord: 144, y_cord: 151)
ConfRoom.create(conf_room: 'Raving Fans',           location_id: 6, x_cord: 503, y_cord: 434)
ConfRoom.create(conf_room: 'Tao',                   location_id: 6, x_cord: 678, y_cord: 355)

ConfRoom.create(conf_room: 'DecisionTech',          location_id: 7, x_cord: 189, y_cord: 262)
ConfRoom.create(conf_room: 'Energy Bus',            location_id: 7, x_cord: 142, y_cord: 470)
ConfRoom.create(conf_room: 'Warrior',               location_id: 7, x_cord: 418, y_cord: 421)
ConfRoom.create(conf_room: 'Net',                   location_id: 7, x_cord: 545, y_cord: 362)
ConfRoom.create(conf_room: 'Strength Finder',       location_id: 7, x_cord: 573, y_cord: 362)

ConfRoom.create(conf_room: 'Break the Rules',       location_id: 8, x_cord: 242, y_cord: 145)
ConfRoom.create(conf_room: 'Acceleration',          location_id: 8, x_cord: 344, y_cord: 145)
ConfRoom.create(conf_room: 'Negotiation',           location_id: 8, x_cord: 705, y_cord: 464)
ConfRoom.create(conf_room: 'Advantage',             location_id: 8, x_cord: 705, y_cord: 526)

ConfRoom.create(conf_room: 'Outliers',              location_id: 9, x_cord: 75,  y_cord: 715)
ConfRoom.create(conf_room: 'Why?',                  location_id: 9, x_cord: 184, y_cord: 666)
ConfRoom.create(conf_room: 'Blueprint',             location_id: 9, x_cord: 187, y_cord: 408)
ConfRoom.create(conf_room: 'Multiplier',            location_id: 9, x_cord: 157, y_cord: 574)
ConfRoom.create(conf_room: 'Disruptive Innovation', location_id: 9, x_cord: 202, y_cord: 574)
ConfRoom.create(conf_room: 'Boom!',                 location_id: 9, x_cord: 180, y_cord: 128)

ConfRoom.create(conf_room: 'Hedgehog',              location_id: 10, x_cord: 172, y_cord: 367)
ConfRoom.create(conf_room: 'Invisible Gorilla',     location_id: 10, x_cord: 172, y_cord: 668)
ConfRoom.create(conf_room: 'Ship',                  location_id: 10, x_cord: 195, y_cord: 740)
ConfRoom.create(conf_room: 'Winning',               location_id: 10, x_cord: 145, y_cord: 740)
