# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160916184126) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "conf_rooms", force: :cascade do |t|
    t.string   "conf_room"
    t.integer  "location_id"
    t.integer  "x_cord"
    t.integer  "y_cord"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "email"
    t.boolean  "current_employee"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "location_id"
    t.integer  "x_cord"
    t.integer  "y_cord"
    t.string   "image"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "phone_number"
    t.string   "extension"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location"
    t.integer  "parent"
    t.string   "image"
    t.string   "has_seats"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "orientation"
  end

  create_table "pointless_feedback_messages", force: :cascade do |t|
    t.string   "name"
    t.string   "email_address"
    t.string   "topic"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
