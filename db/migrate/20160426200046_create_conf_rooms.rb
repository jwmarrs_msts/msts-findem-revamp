class CreateConfRooms < ActiveRecord::Migration
  def change
    create_table :conf_rooms do |t|
      t.string  "conf_room"
      t.integer "location_id"
      t.integer "x_cord"
      t.integer "y_cord"

      t.timestamps null: false
    end
  end
end
