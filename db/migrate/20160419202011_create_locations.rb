class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :location
      t.integer :parent
      t.string :image
      t.string :has_seats

      t.timestamps null: false
    end
  end
end
