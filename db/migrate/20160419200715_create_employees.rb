class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :email
      t.boolean :current_employee
      t.string :first_name
      t.string :last_name
      t.integer :location_id
      t.integer :x_cord
      t.integer :y_cord
      t.string  :image

      t.timestamps null: false
    end
  end
end
