class AddPhoneNumberAndExtensionToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :phone_number, :string
    add_column :employees, :extension, :string
  end
end
