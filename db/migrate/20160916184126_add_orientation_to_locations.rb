class AddOrientationToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :orientation, :integer
  end
end
